

   export function errorFunctionGet(error, module)
    {
      if ( error.response.status == 401)
      {
          this.$router.push('/login')
      }
      if ( error.response.status == 403)
      {
        $notify({
          message:
            '<b>Insufficient Permissions to : ' + module + '</b> - You dont have Access to this Module or Function in the Module. Please Contact Administrator.',
          timeout: 15000,
          icon: 'ni ni-bulb-61',
          type: 'danger',
        });
        this.$router.back()
      }
    }

    export function errorFunctionPost(error, module, code)
    {
      if ( error.response.status == 401)
      {
        this.$router.push('/login')
      }
      else if ( error.response.status == 403)
      {
        this.$notify({
          message:
            '<b>Insufficient Permissions to : ' + module + '</b> - You dont have Access to this Module or Function in the Module. Please Contact Administrator.',
          timeout: 15000,
          icon: 'ni ni-bulb-61',
          type: 'danger',
        });
        this.$router.back()
      }
      else
      {
        this.$notify({
          message:
              '<b>Invalid Input : ' + code + ' </b> - The Code already exists in the system or input doesn\'t fullfill the column requirement. Please choose a different Code or check the Note.',
          timeout: 10000,
          icon: 'ni ni-bulb-61',
          type: 'danger',
        });
      }
    }


