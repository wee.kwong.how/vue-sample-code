import DashboardLayout from '@/views/Layout/DashboardLayout.vue';
import AuthLayout from '@/views/Pages/AuthLayout.vue';
// GeneralViews
import NotFound from '@/views/GeneralViews/NotFoundPage.vue';

// Calendar
const Calendar = () => import(/* webpackChunkName: "extra" */ '@/views/Calendar/Calendar.vue');
// Charts
const Charts = () => import(/* webpackChunkName: "dashboard" */ '@/views/Charts.vue');

// Components pages
const Buttons = () => import(/* webpackChunkName: "components" */ '@/views/Components/Buttons.vue');
const Cards = () => import(/* webpackChunkName: "components" */ '@/views/Components/Cards.vue');
const GridSystem = () => import(/* webpackChunkName: "components" */ '@/views/Components/GridSystem.vue');
const Notifications = () => import(/* webpackChunkName: "components" */ '@/views/Components/Notifications.vue');
const Icons = () => import(/* webpackChunkName: "components" */ '@/views/Components/Icons.vue');
const Typography = () => import(/* webpackChunkName: "components" */ '@/views/Components/Typography.vue');

// Dashboard pages
const Dashboard = () => import(/* webpackChunkName: "dashboard" */ '@/views/Dashboard/Dashboard.vue');
const AlternativeDashboard = () => import(/* webpackChunkName: "dashboard" */ '@/views/Dashboard/AlternativeDashboard.vue');
const Widgets = () => import(/* webpackChunkName: "dashboard" */ '@/views/Widgets.vue');

// Forms pages
const FormElements = () => import(/* webpackChunkName: "forms" */ '@/views/Forms/FormElements.vue');
const FormComponents = () => import(/* webpackChunkName: "forms" */ '@/views/Forms/FormComponents.vue');
const FormValidation = () => import(/* webpackChunkName: "forms" */ '@/views/Forms/FormValidation.vue');

// Maps pages
const GoogleMaps = () => import(/* webpackChunkName: "extra" */ '@/views/Maps/GoogleMaps.vue');
const VectorMaps = () => import(/* webpackChunkName: "extra" */ '@/views/Maps/VectorMaps.vue');

// Pages
const User = () => import(/* webpackChunkName: "pages" */ '@/views/Pages/UserProfile.vue');
const Pricing = () => import(/* webpackChunkName: "pages" */ '@/views/Pages/Pricing.vue');
const TimeLine = () => import(/* webpackChunkName: "pages" */ '@/views/Pages/TimeLinePage.vue');
const Login = () => import(/* webpackChunkName: "pages" */ '@/views/Pages/Login.vue');
const Home = () => import(/* webpackChunkName: "pages" */ '@/views/Pages/Home.vue');
const Register = () => import(/* webpackChunkName: "pages" */ '@/views/Pages/Register.vue');
const Lock = () => import(/* webpackChunkName: "pages" */ '@/views/Pages/Lock.vue');
const Task = () => import(/* webpackChunkName: "pages" */ '@/views/Pages/Lock.vue');
// TableList pages
const RegularTables = () => import(/* webpackChunkName: "tables" */ '@/views/Tables/RegularTables.vue');
const SortableTables = () => import(/* webpackChunkName: "tables" */ '@/views/Tables/SortableTables.vue');
const PaginatedTables = () => import(/* webpackChunkName: "tables" */ '@/views/Tables/PaginatedTables.vue');

// const Milestone = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Setup/Milestone/MilestoneListing.vue');
// const MilestoneNew = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Setup/Milestone/MilestoneNew.vue');
// const MilestoneEdit = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Setup/Milestone/MilestoneEdit.vue');

//const ProjectDashboardListing = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Setup/Project/ProjectDashboardListing.vue');


const SiteDashboard = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Setup/Project/Site/SiteDashboard.vue');
const SiteNew = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Setup/Project/Site/SiteNew.vue');
const SiteEdit = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Setup/Project/Site/SiteEdit.vue');

const ApprovalDashboard = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Setup/Approval/ApprovalDashboard.vue');

// Template
const MilestoneTemplateListing = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Template/Milestone/MilestoneTemplateListing.vue');
const MilestoneTemplateNew = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Template/Milestone/MilestoneTemplateNew.vue');
const MilestoneTemplateEdit = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Template/Milestone/MilestoneTemplateEdit.vue');

const MilestoneTemplateDetailListing = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Template/Milestone/MilestoneTemplateDetailListing.vue');
const MilestoneTemplateDetailNew = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Template/Milestone/MilestoneTemplateDetailNew.vue');
const MilestoneTemplateDetailEdit = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Template/Milestone/MilestoneTemplateDetailEdit.vue');

const DocumentTemplateListing = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Template/Document/DocumentTemplateListing.vue');
const DocumentTemplateNew = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Template/Document/DocumentTemplateNew.vue');
const DocumentTemplateEdit = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Template/Document/DocumentTemplateEdit.vue');

const DocumentTemplateDetailListing = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Template/Document/DocumentTemplateDetailListing.vue');
const DocumentTemplateDetailNew = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Template/Document/DocumentTemplateDetailNew.vue');
const DocumentTemplateDetailEdit = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Template/Document/DocumentTemplateDetailEdit.vue');

const TaskTemplateListing = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Template/Task/TaskTemplateListing.vue');
const TaskTemplateNew = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Template/Task/TaskTemplateNew.vue');
const TaskTemplateEdit = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Template/Task/TaskTemplateEdit.vue');

const TaskTemplateDetailListing = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Template/Task/TaskTemplateDetailListing.vue');
const TaskTemplateDetailNew = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Template/Task/TaskTemplateDetailNew.vue');
const TaskTemplateDetailEdit = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Template/Task/TaskTemplateDetailEdit.vue');

const ProjectDocumentTemplateListing = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Template/ProjectDocument/ProjectDocumentTemplateListing.vue');
const ProjectDocumentTemplateNew = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Template/ProjectDocument/ProjectDocumentTemplateNew.vue');
const ProjectDocumentTemplateEdit = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Template/ProjectDocument/ProjectDocumentTemplateEdit.vue');

const ProjectTaskTemplateListing = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Template/ProjectTask/ProjectTaskTemplateListing.vue');
const ProjectTaskTemplateNew = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Template/ProjectTask/ProjectTaskTemplateNew.vue');
const ProjectTaskTemplateEdit = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Template/ProjectTask/ProjectTaskTemplateEdit.vue');

const ProjectDocumentTemplateDetailListing = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Template/ProjectDocument/ProjectDocumentTemplateDetailListing.vue');
const ProjectDocumentTemplateDetailNew = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Template/ProjectDocument/ProjectDocumentTemplateDetailNew.vue');
const ProjectDocumentTemplateDetailEdit = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Template/ProjectDocument/ProjectDocumentTemplateDetailEdit.vue');

const ProjectTaskTemplateDetailListing = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Template/ProjectTask/ProjectTaskTemplateDetailListing.vue');
const ProjectTaskTemplateDetailNew = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Template/ProjectTask/ProjectTaskTemplateDetailNew.vue');
const ProjectTaskTemplateDetailEdit = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Template/ProjectTask/ProjectTaskTemplateDetailEdit.vue');

// Developer
const DeveloperListing = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Setup/Developer/DeveloperListing.vue');
const DeveloperNew = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Setup/Developer/DeveloperNew.vue');
const DeveloperEdit = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Setup/Developer/DeveloperEdit.vue');

// Developer
const ContractorListing = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Setup/Contractor/ContractorListing.vue');
const ContractorNew = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Setup/Contractor/ContractorNew.vue');
const ContractorEdit = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Setup/Contractor/ContractorEdit.vue');

// Project Type
const ProjectTypeListing = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Setup/ProjectType/ProjectTypeListing.vue');
const ProjectTypeNew = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Setup/ProjectType/ProjectTypeNew.vue');
const ProjectTypeEdit = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Setup/ProjectType/ProjectTypeEdit.vue');

// Project Category
const ProjectCategoryListing = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Setup/ProjectCategory/ProjectCategoryListing.vue');
const ProjectCategoryNew = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Setup/ProjectCategory/ProjectCategoryNew.vue');
const ProjectCategoryEdit = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Setup/ProjectCategory/ProjectCategoryEdit.vue');

// Project
const ProjectListing = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Project/ProjectListing.vue');
const ProjectDashboard = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Project/ProjectDashboard.vue');
const ProjectNew = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Project/ProjectNew.vue');
const ProjectEdit = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Project/ProjectEdit.vue');

// Group
const ProjectGroupDashboard = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/ProjectGroup/ProjectGroupDashboard.vue');
const ProjectGroupListing = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/ProjectGroup/ProjectGroupListing.vue');
const ProjectGroupNew = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/ProjectGroup/ProjectGroupNew.vue');
const ProjectGroupNewFrProject = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/ProjectGroup/ProjectGroupNewFrProject.vue');
const ProjectGroupEdit = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/ProjectGroup/ProjectGroupEdit.vue');

// Site
const ProjectSiteDashboard = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/ProjectSite/ProjectSiteDashboard.vue');
const ProjectSiteListing = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/ProjectSite/ProjectSiteListing.vue');
const ProjectSiteNew = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/ProjectSite/ProjectSiteNew.vue');
const ProjectSiteNewFrProject = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/ProjectSite/ProjectSiteNewFrProject.vue');
const ProjectSiteNewFrGroup = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/ProjectSite/ProjectSiteNewFrGroup.vue');
const ProjectSiteEdit = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/ProjectSite/ProjectSiteEdit.vue');

// Site Contractor
const SiteContractorListing = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/SiteContractor/SiteContractorListing.vue');
const SiteContractorNew = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/SiteContractor/SiteContractorNew.vue');

// Project Milestone
const ProjectMilestoneListing = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/ProjectMilestone/ProjectMilestoneListing.vue');
const ProjectMilestoneNew = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/ProjectMilestone/ProjectMilestoneNew.vue');
const ProjectMilestoneEdit = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/ProjectMilestone/ProjectMilestoneEdit.vue');

// Task
const TaskListing = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Task/TaskListing.vue');
const TaskNew = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Task/TaskNew.vue');
const TaskNewFrProject = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Task/TaskNewFrProject.vue');
const TaskNewFrGroup = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Task/TaskNewFrGroup.vue');
const TaskNewFrSite = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Task/TaskNewFrSite.vue');
const TaskEdit = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Task/TaskEdit.vue');
const TaskEditContractor = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Task/TaskEditContractor.vue');

const ProjectTaskNewFrProject = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/ProjectTask/ProjectTaskNewFrProject.vue');
const ProjectTaskEditFrProject = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/ProjectTask/ProjectTaskEditFrProject.vue');
//ProjectTaskEditFrProject.vue
// Import
const ImportNew = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Setup/Import/ImportNew.vue');
const ImportNewFrProject = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Setup/Import/ImportNewFrProject.vue');
const ImportNewFrGroup = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Setup/Import/ImportNewFrGroup.vue');
const ImportNewFrSite = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Setup/Import/ImportNewFrSite.vue');

// Document
const DocumentListing = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Document/DocumentListing.vue');
const DocumentNew = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Document/DocumentNew.vue');
const DocumentNewFrProject = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Document/DocumentNewFrProject.vue');
const DocumentNewFrGroup = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Document/DocumentNewFrGroup.vue');
const DocumentNewFrSite = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Document/DocumentNewFrSite.vue');
const DocumentEdit = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Document/DocumentEdit.vue');
const DocumentDetailsListing = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/DocumentDetails/DocumentDetailsListing.vue');
const DocumentDetailsNew = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/DocumentDetails/DocumentDetailsNew.vue');

const DocumentCommentListing = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/DocumentComment/DocumentCommentListing.vue');
const DocumentCommentNew = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/DocumentComment/DocumentCommentNew.vue');
const DocumentCommentEdit = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/DocumentComment/DocumentCommentEdit.vue');

const ProjectDocumentNewFrProject = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/ProjectDocument/ProjectDocumentNewFrProject.vue');
const ProjectDocumentEditFrProject = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/ProjectDocument/ProjectDocumentEditFrProject.vue');
const ProjectDocumentDetailsListing = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/ProjectDocument/ProjectDocumentDetailsListing.vue');
const ProjectDocumentApprovalStatus = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/ProjectDocument/ProjectDocumentApprovalStatus.vue');
const ProjectDocumentRecurringListing = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/ProjectDocument/ProjectDocumentRecurringListing.vue');
//ProjectDocumentEditFrProject  ProjectDocumentRecurringListing SiteDocumentListing SiteDocumentNew SiteDocumentRecurringListing

const SiteDocumentListing = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/SiteDocument/SiteDocumentListing.vue');
const SiteDocumentNew = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/SiteDocument/SiteDocumentNew.vue');
const SiteDocumentEdit = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/SiteDocument/SiteDocumentEdit.vue');
const SiteDocumentDetailsListing = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/SiteDocument/SiteDocumentDetailsListing.vue');
const SiteDocumentRecurringListing = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/SiteDocument/SiteDocumentRecurringListing.vue');
const SiteDocumentApprovalStatus = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/SiteDocument/SiteDocumentApprovalStatus.vue');
const SiteDocumentNewFrProject = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/SiteDocument/SiteDocumentNewFrProject.vue');
const SiteDocumentNewFrGroup = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/SiteDocument/SiteDocumentNewFrGroup.vue');
const SiteDocumentNewFrSite = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/SiteDocument/SiteDocumentNewFrSite.vue');

// siteDocumentApprovalStatus SiteDocumentNewFrProject SiteDocumentNewFrGroup
//const ConsolidatedDashboard = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/ConsolidatedDashboard/ConsolidatedDashboard.vue');
const ConsolidatedDashboard = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Dashboard/ConsolidatedDashboard.vue');
const OverviewDashboard = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/ConsolidatedDashboard/OverviewDashboard.vue');

// User
const UserListing = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Setup/User/UserListing.vue');
const UserNew = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Setup/User/UserNew.vue');
const UserEdit = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Setup/User/UserEdit.vue');

const Profile = () => import(/* webpackChunkName: "pages" */ '@/views/Pages/MyUserProfile.vue');

// SAMPLE
const DocumentDetailsRecurringListing = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/DocumentDetails/DocumentDetailsRecurringListing.vue');
const DocumentApprovalStatus = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/DocumentDetails/DocumentApprovalStatus.vue');

// Role
const RoleListing = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Setup/Role/RoleListing.vue');
//const UserNew = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Setup/User/UserNew.vue');
//const UserEdit = () => import(/* webpackChunkName: "pages" */ '@/views/SemCorp/Setup/User/UserEdit.vue');

let templatesMenu = {
  path: '/templates',
  component: DashboardLayout,
  redirect: '/components/buttons',
  name: 'Components',
  children: [
    {
      path: 'projectTaskTemplateListing',
      name: 'Project Task Template Listing',
      meta : { requiresAuth: true },
      component: ProjectTaskTemplateListing
    },
    {
      path: 'projectTaskTemplateNew',
      name: 'New Project Task Template',
      meta : { requiresAuth: true },
      component: ProjectTaskTemplateNew
    },
    {
      path: 'projectTaskTemplateEdit/:id',
      name: 'Edit Project Task Template',
      meta : { requiresAuth: true },
      component: ProjectTaskTemplateEdit
    },

    {
      path: 'projectDocumentTemplateListing',
      name: 'Project Document Template Listing',
      meta : { requiresAuth: true },
      component: ProjectDocumentTemplateListing
    },
    {
      path: 'projectDocumentTemplateNew',
      name: 'New Project Document Template',
      meta : { requiresAuth: true },
      component: ProjectDocumentTemplateNew
    },
    {
      path: 'projectDocumentTemplateEdit/:id',
      name: 'Edit Project Document Template',
      meta : { requiresAuth: true },
      component: ProjectDocumentTemplateEdit
    },

    {
      path: 'projectTaskTemplateDetailListing/:id',
      name: 'Project Task Template Detail Listing',
      meta : { requiresAuth: true },
      component: ProjectTaskTemplateDetailListing
    },
    {
      path: 'projectTaskTemplateDetailNew/:id',
      name: 'New Project Task Template Detail',
      meta : { requiresAuth: true },
      component: ProjectTaskTemplateDetailNew
    },
    {
      path: 'projectTaskTemplateDetailEdit/:id',
      name: 'Edit Project Task Template Detail',
      meta : { requiresAuth: true },
      component: ProjectTaskTemplateDetailEdit
    },

    {
      path: 'projectDocumentTemplateDetailListing/:id',
      name: 'Project Document Template Detail Listing',
      meta : { requiresAuth: true },
      component: ProjectDocumentTemplateDetailListing
    },
    {
      path: 'projectDocumentTemplateDetailNew/:id',
      name: 'New Project Document Template Detail',
      meta : { requiresAuth: true },
      component: ProjectDocumentTemplateDetailNew
    },
    {
      path: 'projectDocumentTemplateDetailEdit/:id',
      name: 'Edit Project Document Template Detail',
      meta : { requiresAuth: true },
      component: ProjectDocumentTemplateDetailEdit
    },

    {
      path: 'milestoneTemplateListing',
      name: 'Milestone Template Listing',
      meta : { requiresAuth: true },
      component: MilestoneTemplateListing
    },
    {
      path: 'milestoneTemplateNew',
      name: 'Milestone Template New',
      meta : { requiresAuth: true },
      component: MilestoneTemplateNew
    },
    {
      path: 'milestoneTemplateEdit/:id',
      name: 'Milestone Template Edit',
      meta : { requiresAuth: true },
      component: MilestoneTemplateEdit
    },
    {
      path: 'milestoneTemplateDetailListing/:id',
      name: 'Milestone Template Detail Listing',
      meta : { requiresAuth: true },
      component: MilestoneTemplateDetailListing
    },
    {
      path: 'milestoneTemplateDetailNew/:id',
      name: 'New Milestone Template Detail',
      meta : { requiresAuth: true },
      component: MilestoneTemplateDetailNew
    },
    {
      path: 'milestoneTemplateDetailEdit/:id',
      name: 'Edit Milestone Template Detail',
      meta : { requiresAuth: true },
      component: MilestoneTemplateDetailEdit
    },

    {
      path: 'documentTemplateListing',
      name: 'Document Template Listing',
      meta : { requiresAuth: true },
      component: DocumentTemplateListing
    },
    {
      path: 'documentTemplateNew',
      name: 'Document Template New',
      meta : { requiresAuth: true },
      component: DocumentTemplateNew
    },
    {
      path: 'documentTemplateEdit/:id',
      name: 'Document Template Edit',
      meta : { requiresAuth: true },
      component: DocumentTemplateEdit
    },

    {
      path: 'documentTemplateDetailListing/:id',
      name: 'Document Template Detail Listing',
      meta : { requiresAuth: true },
      component: DocumentTemplateDetailListing
    },
    {
      path: 'DocumentTemplateDetailNew/:id',
      name: 'New Document Template Detail',
      meta : { requiresAuth: true },
      component: DocumentTemplateDetailNew
    },
    {
      path: 'DocumentTemplateDetailEdit/:id',
      name: 'Edit Document Template Detail',
      meta : { requiresAuth: true },
      component: DocumentTemplateDetailEdit
    },

    {
      path: 'taskTemplateListing',
      name: 'Task Template Listing',
      meta : { requiresAuth: true },
      component: TaskTemplateListing
    },
    {
      path: 'taskTemplateNew',
      name: 'Task Template New',
      meta : { requiresAuth: true },
      component: TaskTemplateNew
    },
    {
      path: 'taskTemplateEdit/:id',
      name: 'Task Template Edit',
      meta : { requiresAuth: true },
      component: TaskTemplateEdit
    },

    {
      path: 'taskTemplateDetailListing/:id',
      name: 'Task Template Detail Listing',
      meta : { requiresAuth: true },
      component: TaskTemplateDetailListing
    },
    {
      path: 'taskTemplateDetailNew/:id',
      name: 'New Task Template Detail',
      meta : { requiresAuth: true },
      component: TaskTemplateDetailNew
    },
    {
      path: 'taskTemplateDetailEdit/:id',
      name: 'Edit Task Template Detail',
      meta : { requiresAuth: true },
      component: TaskTemplateDetailEdit
    }
  ]
};

let setupsMenu = {
  path: '/setups',
  component: DashboardLayout,
  redirect: '/setups/projectType',
  name: 'setups',
  children:
  [
    {
      path: 'developerListing',
      name: 'Developer Listing',
      meta : { requiresAuth: true },
      component: DeveloperListing
    },
    {
      path: 'developerNew',
      name: 'New Developer',
      meta : { requiresAuth: true },
      component: DeveloperNew
    },
    {
      path: 'developerEdit/:id',
      name: 'Edit Developer',
      meta : { requiresAuth: true },
      component: DeveloperEdit
    },

    {
      path: 'contractorListing',
      name: 'Contractor Listing',
      meta : { requiresAuth: true },
      component: ContractorListing
    },
    {
      path: 'contractorNew',
      name: 'New Contractor',
      meta : { requiresAuth: true },
      component: ContractorNew
    },
    {
      path: 'contractorEdit/:id',
      name: 'Edit Contractor',
      meta : { requiresAuth: true },
      component: ContractorEdit
    },

    {
      path: 'projectTypeListing',
      name: 'Project Type Listing',
      meta : { requiresAuth: true },
      component: ProjectTypeListing
    },
    {
      path: 'projectTypeNew',
      name: 'Project Type New',
      meta : { requiresAuth: true },
      component: ProjectTypeNew
    },
    {
      path: 'projectTypeEdit/:id',
      name: 'Project Type Edit',
      meta : { requiresAuth: true },
      component: ProjectTypeEdit
    },

    {
      path: 'projectCategoryListing',
      name: 'Project Category Listing',
      meta : { requiresAuth: true },
      component: ProjectCategoryListing
    },
    {
      path: 'projectCategoryNew',
      name: 'Project Category New',
      meta : { requiresAuth: true },
      component: ProjectCategoryNew
    },
    {
      path: 'projectCategoryEdit/:id',
      name: 'Project Category Edit',
      meta : { requiresAuth: true },
      component: ProjectCategoryEdit
    },
    {
      path: 'UserListing',
      name: 'User Listing',
      meta : { requiresAuth: true },
      component: UserListing
    },
    {
      path: 'UserNew',
      name: 'User New',
      meta : { requiresAuth: true },
      component: UserNew
    },
    {
      path: 'UserEdit/:id',
      name: 'User Edit',
      meta : { requiresAuth: true },
      component: UserEdit
    },
    {
      path: 'RoleListing',
      name: 'Role Listing',
      meta : { requiresAuth: true },
      component: RoleListing
    },

  ]
};

let componentsMenu = {
  path: '/components',
  component: DashboardLayout,
  redirect: '/components/buttons',
  name: 'Components',
  children: [
    {
      path: 'buttons',
      name: 'Buttons',
      meta : { requiresAuth: true },
      component: Buttons
    },
    {
      path: 'cards',
      name: 'Cards',
      meta : { requiresAuth: true },
      component: Cards
    },
    {
      path: 'grid-system',
      name: 'Grid System',
      meta : { requiresAuth: true },
      component: GridSystem
    },
    {
      path: 'notifications',
      name: 'Notifications',
      meta : { requiresAuth: true },
      component: Notifications
    },
    {
      path: 'icons',
      name: 'Icons',
      meta : { requiresAuth: true },
      component: Icons
    },
    {
      path: 'typography',
      name: 'Typography',
      meta : { requiresAuth: true },
      component: Typography
    }
  ]
};

let formsMenu = {
  path: '/forms',
  component: DashboardLayout,
  redirect: '/forms/elements',
  name: 'Forms',
  children: [
    {
      path: 'elements',
      name: 'Form elements',
      meta : { requiresAuth: true },
      component:  FormElements
    },
    {
      path: 'components',
      name: 'Form components',
      meta : { requiresAuth: true },
      component:  FormComponents
    },
    {
      path: 'validation',
      name: 'Form validation',
      meta : { requiresAuth: true },
      component:  FormValidation
    }
  ]
};

let tablesMenu = {
  path: '/tables',
  component: DashboardLayout,
  redirect: '/table/regular',
  name: 'Tables menu',
  children: [
    {
      path: 'regular',
      name: 'Tables',
      meta : { requiresAuth: true },
      component: RegularTables
    },
    {
      path: 'sortable',
      name: 'Sortable',
      meta : { requiresAuth: true },
      component: SortableTables
    },
    {
      path: 'paginated',
      name: 'Paginated Tables',
      meta : { requiresAuth: true },
      component: PaginatedTables
    }
  ]
};

let mapsMenu = {
  path: '/maps',
  component: DashboardLayout,
  name: 'Maps',
  redirect: '/maps/google',
  children: [
    {
      path: 'google',
      name: 'Google Maps',
      meta : { requiresAuth: true },
      component: GoogleMaps
    },
    {
      path: 'vector',
      name: 'Vector Map',
      meta : { requiresAuth: true },
      component: VectorMaps
    }
  ]
};

let pagesMenu = {
  path: '/pages',
  component: DashboardLayout,
  name: 'Pages',
  redirect: '/pages/user',
  children: [
    {
      path: 'user',
      name: 'User Page',
      meta : { requiresAuth: true },
      component: User
    },
    {
      path: 'timeline',
      name: 'Timeline Page',
      meta : { requiresAuth: true },
      component: TimeLine
    },
    {
      path: 'profile',
      name: 'Profile Page',
      meta : { requiresAuth: true },
      component: Profile
    },
  ]
};



let authPages = {
  path: '/',
  component: AuthLayout,
  name: 'Authentication',
  children: [
    {
      path: '/home',
      name: 'Home',
      component: Home,
      meta: {
        noBodyBackground: true
      }
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    },
    {
      path: '/pricing',
      name: 'Pricing',
      component: Pricing
    },
    {
      path: '/lock',
      name: 'Lock',
      component: Lock
    },
    {
      path: '/taskModule',
      name: 'Task',
      component: Task
    },
    { path: '*', component: NotFound }
  ]
};

const routes = [
  {
    path: '/',
    redirect: '/home',
    name: 'Home'
  },
  componentsMenu,
  formsMenu,
  tablesMenu,
  mapsMenu,
  pagesMenu,
  templatesMenu,
  setupsMenu,
  {
    path: '/',
    component: DashboardLayout,
    redirect: '/projectListing',
    name: 'iTask',
    meta: {
      requiresAuth: true,
    },

    children: [
      {
        path: 'documentApprovalStatus/:id',
        name: 'Document Approval Status',
        meta : { requiresAuth: true },
        component: DocumentApprovalStatus
      },
      {
        path: 'documentDetailsRecurringListing/:id',
        name: 'Document Recurring Listing',
        meta : { requiresAuth: true },
        component: DocumentDetailsRecurringListing
      },
      {
        path: 'consolidatedDashboard',
        name: 'Consolidated Dashboard',
        meta : { requiresAuth: true },
        component: ConsolidatedDashboard
      },
      {
        path: 'overviewDashboard',
        name: 'Overview Dashboard',
        meta : { requiresAuth: true },
        component: OverviewDashboard
      },
      {
        path: 'dashboard',
        name: 'Dashboard',
        meta : { requiresAuth: true },
        component: Dashboard
      },
      {
        path: 'alternative',
        name: 'Alternative',
        component: AlternativeDashboard,
        meta: {
          navbarType: 'light',
          requiresAuth: true,
        }
      },
      {
        path: 'calendar',
        name: 'Calendar',
        meta : { requiresAuth: true },
        component: Calendar
      },
      {
        path: 'charts',
        name: 'Charts',
        meta : { requiresAuth: true },
        component: Charts
      },
      {
        path: 'widgets',
        name: 'Widgets',
        meta : { requiresAuth: true },
        component: Widgets
      },

      {
        path: 'siteDashboard',
        name: 'Site Dashboard',
        meta: {
          navbarType: 'light',
          requiresAuth: true,
        },
        component: SiteDashboard
      },
      {
        path: 'siteNew',
        name: 'New Site',
        meta : { requiresAuth: true },
        component: SiteNew
      },
      {
        path: 'siteEdit',
        name: 'Edit Site',
        meta : { requiresAuth: true },
        component: SiteEdit
      },
      {
        path: 'approvalDashboard',
        name: 'Approval Dashboard',
        meta : { requiresAuth: true },
        component: ApprovalDashboard
      },

      {
        path: 'projectListing',
        name: 'Project Listing',
        meta : { requiresAuth: true },
        component: ProjectListing
      },
      {
        path: 'projectDashboard/:id',
        name: 'Project Dashboard',
        meta : { requiresAuth: true },
        component: ProjectDashboard
      },
      {
        path: 'projectNew',
        name: 'New Project',
        meta : { requiresAuth: true },
        component: ProjectNew
      },
      {
        path: 'projectEdit/:id',
        name: 'Edit Project',
        meta : { requiresAuth: true },
        component: ProjectEdit
      },

      {
        path: 'projectGroupListing',
        name: 'Project Group Listing',
        meta : { requiresAuth: true },
        component: ProjectGroupListing
      },
      {
        path: 'projectGroupDashboard/:id',
        name: 'Project GroupDashboard',
        meta : { requiresAuth: true },
        component: ProjectGroupDashboard
      },
      {
        path: 'projectGroupNew',
        name: 'New Project Group',
        meta : { requiresAuth: true },
        component: ProjectGroupNew
      },
      {
        path: 'projectGroupNewFrProject/:id',
        name: 'New Project Group',
        meta : { requiresAuth: true },
        component: ProjectGroupNewFrProject
      },
      {
        path: 'projectGroupEdit/:id',
        name: 'Edit Project Group',
        meta : { requiresAuth: true },
        component: ProjectGroupEdit
      },

      {
        path: 'projectSiteListing',
        name: 'Project Site Listing',
        meta : { requiresAuth: true },
        component: ProjectSiteListing
      },
      {
        path: 'projectSiteDashboard/:id',
        name: 'Project Site Dashboard',
        meta : { requiresAuth: true },
        component: ProjectSiteDashboard
      },
      {
        path: 'projectSiteNew',
        name: 'New Project Site',
        meta : { requiresAuth: true },
        component: ProjectSiteNew
      },
      {
        path: 'projectSiteNewFrProject/:id',
        name: 'New Project Site',
        meta : { requiresAuth: true },
        component: ProjectSiteNewFrProject
      },
      {
        path: 'projectSiteNewFrGroup/:id',
        name: 'New Project Site',
        meta : { requiresAuth: true },
        component: ProjectSiteNewFrGroup
      },
      {
        path: 'projectSiteEdit/:id',
        name: 'Edit Project Site',
        meta : { requiresAuth: true },
        component: ProjectSiteEdit
      },

      {
        path: 'projectMilestoneListing/:id',
        name: 'Project Milestone Listing',
        meta : { requiresAuth: true },
        component: ProjectMilestoneListing
      },
      {
        path: 'projectMilestoneNew/:id',
        name: 'New Project Milestone',
        meta : { requiresAuth: true },
        component: ProjectMilestoneNew
      },
      {
        path: 'projectMilestoneEdit/:id',
        name: 'Edit Project Milestone',
        meta : { requiresAuth: true },
        component: ProjectMilestoneEdit
      },

      {
        path: 'siteContractorListing/:id',
        name: 'Site Contractor Listing',
        meta : { requiresAuth: true },
        component: SiteContractorListing
      },

      {
        path: 'siteContractorNew/:id',
        name: 'New Site Contractor ',
        meta : { requiresAuth: true },
        component: SiteContractorNew
      },

      {
        path: 'taskListing',
        name: 'Task Listing',
        meta : { requiresAuth: true },
        component: TaskListing
      },
      {
        path: 'taskNew',
        name: 'New Site Task',
        meta : { requiresAuth: true },
        component: TaskNew
      },
      {
        path: 'taskNewFrProject/:id',
        name: 'New Task',
        meta : { requiresAuth: true },
        component: TaskNewFrProject
      },
      {
        path: 'projectTaskNewFrProject/:id',
        name: 'New Project Task ',
        meta : { requiresAuth: true },
        component: ProjectTaskNewFrProject
      },
      {
        path: 'projectTaskEditFrProject/:id',
        name: 'Edit Project Task ',
        meta : { requiresAuth: true },
        component: ProjectTaskEditFrProject
      },

      {
        path: 'taskNewFrGroup/:id',
        name: 'New Task',
        meta : { requiresAuth: true },
        component: TaskNewFrGroup
      },
      {
        path: 'taskNewFrSite/:id',
        name: 'New Task',
        meta : { requiresAuth: true },
        component: TaskNewFrSite
      },
      {
        path: 'taskEdit/:id',
        name: 'Edit Task',
        meta : { requiresAuth: true },
        component: TaskEdit
      },
      {
        path: 'taskEditContractor/:id',
        name: 'Edit Task Contractor',
        meta : { requiresAuth: true },
        component: TaskEditContractor
      },


      {
        path: 'importNew',
        name: 'New Import',
        meta : { requiresAuth: true },
        component: ImportNew
      },
      {
        path: 'importNewFrProject/:id',
        name: 'New Import For Project',
        meta : { requiresAuth: true },
        component: ImportNewFrProject
      },
      {
        path: 'importNewFrGroup/:id',
        name: 'New Import For Group',
        meta : { requiresAuth: true },
        component: ImportNewFrGroup
      },
      {
        path: 'importNewFrSite/:id',
        name: 'New Import For Site',
        meta : { requiresAuth: true },
        component: ImportNewFrSite
      },

      {
        path: 'projectDocumentNewFrProject/:id',
        name: 'New Project Document ',
        meta : { requiresAuth: true },
        component: ProjectDocumentNewFrProject
      },
      {
        path: 'projectDocumentEditFrProject/:id',
        name: 'New Project Document ',
        meta : { requiresAuth: true },
        component: ProjectDocumentEditFrProject
      },
      {
        path: 'projectDocumentDetailsListing/:id/:id2',
        name: 'Project Document Details Information',
        meta : { requiresAuth: true },
        component: ProjectDocumentDetailsListing
      },
      {
        path: 'projectDocumentRecurringListing/:id',
        name: 'Project Document Recurring Listing',
        meta : { requiresAuth: true },
        component: ProjectDocumentRecurringListing
      },
      {
        path: 'projectDocumentApprovalStatus/:id',
        name: 'Project Document Approval Status',
        meta : { requiresAuth: true },
        component: ProjectDocumentApprovalStatus
      },

      {
        path: 'siteDocumentListing',
        name: 'Site Document Listing',
        meta : { requiresAuth: true },
        component: SiteDocumentListing
      },
      {
        path: 'siteDocumentNew',
        name: 'New Site Document ',
        meta : { requiresAuth: true },
        component: SiteDocumentNew
      },
      {
        path: 'siteDocumentEdit/:id',
        name: 'Edit Site Document ',
        meta : { requiresAuth: true },
        component: SiteDocumentEdit
      },
      {
        path: 'siteDocumentDetailsListing/:id/:id2',
        name: 'Site Document Details Information',
        meta : { requiresAuth: true },
        component: SiteDocumentDetailsListing
      },

      {
        path: 'siteDocumentRecurringListing/:id',
        name: 'Site Document Recurring Listing',
        meta : { requiresAuth: true },
        component: SiteDocumentRecurringListing
      },
      {
        path: 'siteDocumentApprovalStatus/:id',
        name: 'Site Document Approval Status',
        meta : { requiresAuth: true },
        component: SiteDocumentApprovalStatus
      },
      {
        path: 'siteDocumentNewFrProject/:id',
        name: 'New Site Document ',
        meta : { requiresAuth: true },
        component: SiteDocumentNewFrProject
      },
      {
        path: 'siteDocumentNewFrGroup/:id',
        name: 'New Site Document ',
        meta : { requiresAuth: true },
        component: SiteDocumentNewFrGroup
      },
      {
        path: 'siteDocumentNewFrSite/:id',
        name: 'New Site Document ',
        meta : { requiresAuth: true },
        component: SiteDocumentNewFrSite
      },

      /****************************************************** */



      {
        path: 'documentListing',
        name: 'Document Listing',
        meta : { requiresAuth: true },
        component: DocumentListing
      },
      {
        path: 'documentNew',
        name: 'New Document',
        meta : { requiresAuth: true },
        component: DocumentNew
      },
      {
        path: 'documentEdit/:id',
        name: 'Edit Document',
        meta : { requiresAuth: true },
        component: DocumentEdit
      },
      {
        path: 'documentNewFrProject/:id',
        name: 'New Document From Project',
        meta : { requiresAuth: true },
        component: DocumentNewFrProject
      },
      {
        path: 'documentNewFrGroup/:id',
        name: 'New Document From Group',
        meta : { requiresAuth: true },
        component: DocumentNewFrGroup
      },
      {
        path: 'documentNewFrSite/:id',
        name: 'New Document From Site',
        meta : { requiresAuth: true },
        component: DocumentNewFrSite
      },

      {
        path: 'documentDetailsListing/:id',
        name: 'Document Details Listing',
        meta : { requiresAuth: true },
        component: DocumentDetailsListing
      },
      {
        path: 'documentDetailsNew/:id',
        name: 'Document Details New',
        meta : { requiresAuth: true },
        component: DocumentDetailsNew
      },

      {
        path: 'documentCommentListing/:id',
        name: 'Document Comment Listing',
        meta : { requiresAuth: true },
        component: DocumentCommentListing
      },
      {
        path: 'documentCommentNew/:id',
        name: 'New Document Comment',
        meta : { requiresAuth: true },
        component: DocumentCommentNew
      },
      {
        path: 'documentCommentEdit/:id',
        name: 'Edit Document Comment',
        meta : { requiresAuth: true },
        component: DocumentCommentEdit
      },



    ]
  },
  authPages,
];

export default routes;
