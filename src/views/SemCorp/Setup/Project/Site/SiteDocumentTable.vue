<template>
    <b-card no-body class="bg-default shadow">
        <b-card-header class="bg-transparent border-0">
            <h3 class="mb-0 text-white">One-Time Document Listing in SN-NT - Tengeh Reservoir North Site for (TRTP-2398 - Tengeh Reservoir Tender Project)</h3>
            <br>
            <div class="text-right">
                <base-button type="neutral" size="sm">Add New Classification</base-button>
                <base-button type="neutral" size="sm">Addtional Document Request</base-button>
            </div>
            <div class="text-right">

            </div>
        </b-card-header>

        <el-table class="table-responsive table-dark"
                  header-row-class-name="thead-dark"
                  :data="documents">
            <el-table-column label="Classification"
                             min-width="130px"
                             prop="name"
                             sortable>
                <template v-slot="{row}">
                    <b-media no-body class="align-items-center">
                        <b-media-body>
                            <span class="font-weight-600 name mb-0 text-sm text-white">{{row.taskId}}</span>
                        </b-media-body>
                    </b-media>
                </template>
            </el-table-column>

            <el-table-column label="Contractor"
                             min-width="100px"
                             prop="name"
                             sortable>
                <template v-slot="{row}">
                    <b-media no-body class="align-items-center">
                        <b-media-body>
                            <span class="font-weight-600 name mb-0 text-sm text-white">{{row.taskName}}</span>
                        </b-media-body>
                    </b-media>
                </template>
            </el-table-column>

            <el-table-column label="Status"
                             min-width="120px"
                             prop="status"
                             sortable>
                <template v-slot="{row}">
                    <badge class="badge-dot mr-4" type="">

                        <span class="status text-white">{{row.taskStatus}}</span>
                    </badge>
                </template>
            </el-table-column>

            <el-table-column label="Revision"
                             min-width="90px"
                             prop="status"
                             sortable>
                <template v-slot="{row}">
                    <badge class="badge-dot mr-4" type="">

                        <span class="status text-white">{{row.taskRevisionNo}}</span>
                    </badge>
                </template>
            </el-table-column>

            <el-table-column label="Mandatory"
                             min-width="100px"
                             prop="status"
                             sortable>
                <template v-slot="{row}">
                    <badge class="badge-dot mr-4" type="">
                        <i :class="`bg-${row.taskType}`"></i>
                        <span class="status text-white">{{row.taskMandatory}}</span>
                    </badge>
                </template>
            </el-table-column>

            <el-table-column label="Updated At"
                             min-width="80px"
                             prop="status"
                             sortable>
                <template v-slot="{row}">
                    <badge class="badge-dot mr-4" type="">

                        <span class="status text-white">{{row.taskDate}}</span>
                    </badge>
                </template>
            </el-table-column>

            <el-table-column label="Action" min-width="160px">
               <template v-slot="">
                    <div class="d-flex align-items-center">
                        <div>
                            <base-button type="primary" size="sm" @click="newModal()">Upload Document</base-button>
                        </div>
                        &nbsp;
                        <div>
                            <base-button size="sm" type="primary" @click="editModal()">Request</base-button>
                        </div>
                    </div>
                </template>
            </el-table-column>
        </el-table>

        <b-card-footer class="py-2 d-flex justify-content-end bg-transparent">
            <base-pagination v-model="currentPage" :per-page="10" :total="50"></base-pagination>
        </b-card-footer>

        <modal :show.sync="modals.form"
                   size="lg"
                   body-classes="p-0">
              <card type="secondary"
                    header-classes="bg-transparent pb-5"
                    body-classes="px-lg-5 py-lg-5"
                    class="border-0 mb-0">
                <template>
                  <div class="text-center text-muted mb-4">
                    <h2>{{ formModal.title }} Document to SN-NT - Tengeh Reservoir North Site for (TRTP-2398 - Tengeh Reservoir Tender Project)</h2>
                  </div>
                  <b-form role="form">
                    <base-input label="Classification" name="Classification" placeholder="Classification" v-model="formModal.classification" disabled>
                    </base-input>
                    <base-input label="Contractor" name="Contractor" placeholder="Contractor" v-model="formModal.contactor" disabled>
                    </base-input>
                    <!-- <base-input label="Contractor" name="Contractor">
                      <select class="form-control" v-model="formModal.contactor">
                        <option value="" disabled>-- Please Select A Contractor --</option>
                        <option value="Solar Ex">Solar Ex</option>
                        <option value="Long Seng">Long Seng</option>
                      </select>
                    </base-input> -->
                    <!-- <base-input label="Status" name="Status">
                      <select class="form-control" v-model="formModal.status">
                        <option value="" disabled>-- Please Select A Status --</option>
                        <option value="Pending">Pending</option>
                        <option value="Waiting For Approval">Waiting For Approval</option>
                        <option value="Required Revision">Required Revision</option>
                      </select>
                    </base-input> -->
                     <h4>Attachment</h4>
                    <dropzone-file-upload v-model="formModal.doc_attachment" multiple></dropzone-file-upload>
                    <!-- <b-form-checkbox v-model="formModal.mandatory"
                    value="Mandatory"
                    unchecked-value="Not_Mandatory">Mandatory</b-form-checkbox> -->
                    <div class="text-center">
                      <base-button type="primary" class="my-4" @click="notifyUpload()">{{ formModal.buttonName }}</base-button>
                      <base-button type="primary" class="my-4" @click="clearModal()">Cancel</base-button>
                    </div>
                  </b-form>
                </template>
              </card>
        </modal>

        <modal :show.sync="modals.formRequest"
                   size="lg"
                   body-classes="p-0">
              <card type="secondary"
                    header-classes="bg-transparent pb-5"
                    body-classes="px-lg-5 py-lg-5"
                    class="border-0 mb-0">
                <template>
                  <div class="text-center text-muted mb-4">
                    <h2>Requesition for Document in SN-NT - Tengeh Reservoir North Site for (TRTP-2398 - Tengeh Reservoir Tender Project)</h2>
                  </div>
                  <b-form role="form">
                    <base-input label="Classification" name="Classification" placeholder="Classification" v-model="formModal.classification" disabled>
                    </base-input>
                    <base-input label="Contractor" name="Contractor" placeholder="Contractor" v-model="formModal.contactor" disabled>
                    </base-input>
                    <base-input label="Please State Your Request & Reason" name="Request & Reason" placeholder="Request" v-model="formModal.request">
                    </base-input>
                    <!-- <base-input label="Contractor" name="Contractor">
                      <select class="form-control" v-model="formModal.contactor">
                        <option value="" disabled>-- Please Select A Contractor --</option>
                        <option value="Solar Ex">Solar Ex</option>
                        <option value="Long Seng">Long Seng</option>
                      </select>
                    </base-input>
                    <base-input label="Status" name="Status">
                      <select class="form-control" v-model="formModal.status">
                        <option value="" disabled>-- Please Select A Status --</option>
                        <option value="Pending">Pending</option>
                        <option value="Waiting For Approval">Waiting For Approval</option>
                        <option value="Required Revision">Required Revision</option>
                      </select>
                    </base-input> -->
                     <!-- <h4>Attachment</h4>
                    <dropzone-file-upload v-model="formModal.doc_attachment" multiple></dropzone-file-upload> -->
                    <!-- <b-form-checkbox v-model="formModal.mandatory"
                    value="Mandatory"
                    unchecked-value="Not_Mandatory">Mandatory</b-form-checkbox> -->
                    <div class="text-center">
                      <base-button type="primary" class="my-4" @click="notifyVue()">Request</base-button>
                      <base-button type="primary" class="my-4" @click="clearModal()">Cancel</base-button>
                    </div>
                  </b-form>
                </template>
              </card>
        </modal>
    </b-card>

</template>

<script>

  import documents from './js/documents.js'
  import { Table, TableColumn, DropdownMenu, DropdownItem, Dropdown} from 'element-ui'
  import {homeLink} from '@/assets/js/config.js'
  import { Modal } from '@/components';
  import DropzoneFileUpload from '@/components/Inputs/DropzoneFileUpload'

  export default {
    name: 'site-document-table',
    components: {
      Modal,
      [Table.name]: Table,
      [TableColumn.name]: TableColumn,
      [Dropdown.name]: Dropdown,
      [DropdownItem.name]: DropdownItem,
      [DropdownMenu.name]: DropdownMenu,
      DropzoneFileUpload,
    },
    data() {
      return {
        modals: {
          form: false,
          formRequest: false,
        },
        formModal: {
          classification: '',
          contactor: '',
          status: '',
          doc_attachment: [],
          title: 'Add New',
          buttonName: 'Add New Document',
          mandatorty : 'Not_Mandatory',
          request: '',
        },
        documents,
        currentPage: 1,
        img1: homeLink+'/img/theme/team-1.jpg',
      };
    },
    methods: {
       newModal(){

          this.formModal.contactor = 'Solar Ex'
          this.formModal.classification = 'Action Item'
          this.formModal.status = ''
          this.formModal.buttonName = 'Upload New Document'
          this.formModal.title = 'Upload Document'
          this.formModal.doc_attachment = []
          this.formModal.mandatorty = 'Not_Mandatory'
          this.formModal.request = '',
          this.modals.form = true
      },
      editModal(){

          this.formModal.contactor = 'Solar Ex'
          this.formModal.classification = 'Action Item'
          this.formModal.status = 'Waiting For Approval'
          this.formModal.buttonName = 'Update Document'
          this.formModal.title = 'Edit Document'
          this.formModal.doc_attachment = []
          this.formModal.mandatorty = 'Mandatory'
          this.formModal.request = '',
          this.modals.formRequest = true
      },
      notifyVue() {
        if ( this.formModal.request === ''  )
        {
          this.$notify({
          message:
            '<b>Request Input Error : </b> - Please fill Request & Reason.',
          timeout: 10000,
          icon: 'ni ni-bulb-61',
          type: 'danger',
          });
        }
        else
        {
          this.$notify({
          message:
            '<b>New Request : Action Item (' + this.formModal.request + ') : </b> - Successfully Sent to Project Manager. Pending Approval',
          timeout: 10000,
          icon: 'ni ni-bell-55',
          type: 'default',
          });
          this.clearModal()
        }
      },
      notifyUpload() {
        if ( this.formModal.doc_attachment.length === 0 )
        {
          this.$notify({
          message:
            '<b>Request Input Error : </b> - Please at least upload an attchment',
          timeout: 10000,
          icon: 'ni ni-bulb-61',
          type: 'danger',
          });
        }
        else
        {
          this.$notify({
          message:
            '<b>New One-Time Document (Action Item) : </b> - Succesfully Uploaded, System will auto adjust the Revision Number.',
          timeout: 10000,
          icon: 'ni ni-bell-55',
          type: 'default',
          });
          this.clearModal()
        }
      },
      clearModal()
      {
        this.modals.form = false
        this.modals.formRequest = false
        this.formModal.contactor = ''
        this.formModal.classification = ''
        this.formModal.status = ''
        this.formModal.request = '',
        this.formModal.doc_attachment = []
      },
    }
  }
</script>
