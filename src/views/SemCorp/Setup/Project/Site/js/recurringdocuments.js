
export default [
  {
    taskId: 'Daily Manpower Update',
    taskName: 'Solar Ex',
    taskMilestone: 'Kick-Off',
    taskStatus: 'Waiting For Approval',
    taskFrequency: 'Daily',
    taskTotal: '4',
    taskDate: '01-Jan-2021'
  },
  {
    taskId: 'Daily PTW',
    taskName: 'Solar Ex',
    taskMilestone: 'Design',
    taskStatus: 'Waiting For Approval',
    taskFrequency: 'Daily',
    taskTotal: '4',
    taskDate: '01-Jan-2021'
  },
  {
    taskId: 'Daily Safety Checklist',
    taskName: 'Solar Ex',
    taskMilestone: 'Installation',
    taskStatus: 'Approved',
    taskFrequency: 'Daily',
    taskTotal: '9',
    taskDate: '05-Jan-2021'
  },
  {
    taskId: 'Weekly Progres Report',
    taskName: 'Solar Ex',
    taskMilestone: 'Installation',
    taskStatus: 'Approved',
    taskFrequency: 'Weekly',
    taskTotal: '1',
    taskDate: '05-Jan-2021'
  },
  {
    taskId: 'Daily Manpower Update',
    taskName: 'Long Seng',
    taskMilestone: 'Kick-Off',
    taskStatus: 'Waiting For Approval',
    taskFrequency: 'Daily',
    taskTotal: '9',
    taskDate: '05-Jan-2021'
  },
  {
    taskId: 'Daily PTW',
    taskName: 'Long Seng',
    taskMilestone: 'Design',
    taskStatus: 'Waiting For Approval',
    taskFrequency: 'Daily',
    taskTotal: '9',
    taskDate: '05-Jan-2021'
  },
  {
    taskId: 'Daily Safety Checklist',
    taskName: 'Long Seng',
    taskMilestone: 'Installation',
    taskStatus: 'Approved',
    taskFrequency: 'Daily',
    taskTotal: '9',
    taskDate: '05-Jan-2021'
  },
  {
    taskId: 'Weekly Progres Report',
    taskName: 'Long Seng',
    taskMilestone: 'Installation',
    taskStatus: 'Approved',
    taskFrequency: 'Weekly',
    taskTotal: '1',
    taskDate: '05-Jan-2021'
  },

]
