
export default [
  {
    code: 'TRTP-2398',
    title: 'Tengeh Reservoir Tender Project',
    budget: '$2500 USD',
    status: 'pending',
    statusType: 'warning',
    manager: 'Alex',
    completion: 34
  },
  {
    code: 'MNTP-8650',
    title: 'Menara TM Project',
    budget: '$1800 USD',
    status: 'completed',
    statusType: 'success',
    manager: 'James',
    completion: 100
  },
  {
    code: 'SGLP-3564',
    title: 'Sunway Geo Lake Project',
    budget: '$3150 USD',
    status: 'delayed',
    statusType: 'danger',
    manager: 'Aida',
    completion: 72
  },
  {
    code: 'PCWM-1132',
    title: 'PJ Causeway Mall Project',
    budget: '$4400 USD',
    status: 'on schedule',
    statusType: 'info',
    manager: 'Nicole',
    completion: 90
  }
]
