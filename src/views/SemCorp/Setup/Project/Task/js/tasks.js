
export default [
  {
    taskId: 'TRTP-000001',
    taskSiteName: 'SN-NT',
    taskName: 'Handover Requesition Document For North',
    taskMilestone: 'Kick-Off',
    taskStatus: 'W.I.P',
    taskProgress: 50,
    taskStatusType: 'info'
  },
  {
    taskId: 'TRTP-000002',
    taskSiteName: 'SN-SH',
    taskName: 'Handover Requesition Document For South',
    taskMilestone: 'Kick-Off',
    taskStatus: 'Complete',
    taskProgress: 100,
    taskStatusType: 'success'
  },
  {
    taskId: 'TRTP-000003',
    taskSiteName: 'SN-ET',
    taskName: 'Handover Requesition Document For East',
    taskMilestone: 'Kick-Off',
    taskStatus: 'W.I.P',
    taskProgress: 20,
    taskStatusType: 'danger'
  },
  {
    taskId: 'TRTP-000004',
    taskSiteName: 'SN-WT',
    taskName: 'Handover Requesition Document For West',
    taskMilestone: 'Kick-Off',
    taskStatus: 'Complete',
    taskProgress: 100,
    taskStatusType: 'success'
  },

  {
    taskId: 'TRTP-000005',
    taskSiteName: 'SN-NT',
    taskName: 'Handover Requesition Document For North',
    taskMilestone: 'Kick-Off',
    taskStatus: 'W.I.P',
    taskProgress: 50,
    taskStatusType: 'info'
  },
  {
    taskId: 'TRTP-000006',
    taskSiteName: 'SN-SH',
    taskName: 'Handover Requesition Document For South',
    taskMilestone: 'Kick-Off',
    taskStatus: 'Complete',
    taskProgress: 100,
    taskStatusType: 'success'
  },
  {
    taskId: 'TRTP-000007',
    taskSiteName: 'SN-ET',
    taskName: 'Handover Requesition Document For East',
    taskMilestone: 'Kick-Off',
    taskStatus: 'W.I.P',
    taskProgress: 20,
    taskStatusType: 'danger'
  },
  {
    taskId: 'TRTP-000008',
    taskSiteName: 'SN-WT',
    taskName: 'Handover Requesition Document For West',
    taskMilestone: 'Kick-Off',
    taskStatus: 'Complete',
    taskProgress: 100,
    taskStatusType: 'success'
  },


]
