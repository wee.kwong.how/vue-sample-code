
export default [
  {
    milestoneName: 'Kick-Off',
    milestoneInfo: 'Use for preparation on starting of Project',
    status: 'Active',
    statusType: 'success',
    createdBy: 'Admin',
    createdDate: '01-MAR-2021'
  },
  {
    milestoneName: 'Design',
    milestoneInfo: 'Use for all Design related Task',
    status: 'Active',
    statusType: 'success',
    createdBy: 'Admin',
    createdDate: '01-MAR-2021'
  },
  {
    milestoneName: 'Installation',
    milestoneInfo: 'Use for all Installation Task',
    status: 'Active',
    statusType: 'success',
    createdBy: 'Admin',
    createdDate: '01-MAR-2021'
  },
  {
    milestoneName: 'Inspection',
    milestoneInfo: 'Use for all Inspection After Installation',
    status: 'Active',
    statusType: 'success',
    createdBy: 'Admin',
    createdDate: '01-MAR-2021'
  },
  {
    milestoneName: 'Completed',
    milestoneInfo: 'Unused',
    status: 'Active',
    statusType: 'success',
    createdBy: 'Admin',
    createdDate: '01-MAR-2021'
  },
  {
    milestoneName: 'Closed',
    milestoneInfo: 'Unused',
    status: 'Disabled',
    statusType: 'danger',
    createdBy: 'Admin',
    createdDate: '01-MAR-2021'
  }
]
